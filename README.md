approved by Brad and Kent

Rubric turned in on time (5 points)
Remember to get it checked out by a TA!

Languages/Frameworks used (10 points)
10 - Learned/Used Vue.js frontend
0 - PHP backend 
0 - MySQL Database

Functionality (62 points)
Users can register, login, and logout (10 points)
Users can access to their own money book when logged in (5 points)
Each user can create multiple money books with different purposes (7 points)
Users can edit the name of each book (3 points)
Users can post/edit/delete their expenses (5 points)
Users can post/edit/delete their income (5 points)
Users can see the list of monthly money books (4 points)
Can view when click one from the list (4 points)
In case of travel, currency exchanger is provided (6 points)
Net balance of each month is calculated and presented in the list (6 points)
All actions are performed over Vue.js (api.php), without ever needing to reload the page (6 points)

Best Practices (5 points)
Code is readable and well formatted (3 points)
Passwords are hashed, salted, and checked securely (2 points)

Usability (3 points) 
Site is easy to navigate and intuitive (3 points)

Creative Portion (15 points)
Users can view the expense/income list in certain orders (date ascending / descending, alphabetical order, amount order.. )
Users can categorize their expenses/income when posting.
Users can search through categories.
Users can choose to see income/expense


